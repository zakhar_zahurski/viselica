﻿using System.IO;

class MainClass1
{
    public static void Main(string[] args)
    {
        string path = @"C:\Users\zakhar\source\repos\Viselica\word_rus.txt";
        string[] wordsList = File.ReadAllLines(path);        
        Random random = new Random();
        string[] render =
        {
            "   \n\n \n \n \n \n ===============",
            "   \n     |\n     |\n     |\n     |\n     |\n     |\n ===============",
            "     _________\n     |\n     |\n     |\n     |\n     | \n     |\n ===============",
            "     _________\n     |       |\n     |       |\n     |\n     |\n     | \n     |\n ===============",
            "     _________\n     |       |\n     |       |\n     |       0\n     |\n     | \n     |\n ===============",
            "     _________\n     |       |\n     |       |\n     |       0\n     |      /|\\ \n     | \n     |\n ===============",
            "     _________\n     |       |\n     |       |\n     |       0\n     |      /|\\ \n     |      / \\  \n     | \n ===============",
        };

        while (true)
        {
            string word = wordsList[random.Next(0, wordsList.Length)];
            Console.WriteLine($"Слово состоит из {word.Length} букв");
            char[] viewWord = new char[word.Length];
            for (int i = 0; i < word.Length; i++)
            {
                viewWord[i] = '_';
            }
            Console.WriteLine(viewWord);

            int errorCount = 7;
            int freeLetters = word.Length;
            while(errorCount > 0 && freeLetters > 0)
            {
                Console.WriteLine("Введите букву");
                string letter = Console.ReadLine();
                int guessed = 0;
                if (!char.IsLetter(letter[0]) )
                {
                    Console.Clear();
                    Console.WriteLine("Введите букву");
                    continue;
                }
                for (int k = 0; k < viewWord.Length; k++)
                {
                    if (letter[0] == word[k])
                    {
                        viewWord[k] = letter[0];
                        guessed++;
                        freeLetters--;
                        continue;
                    }                    
                }

                if(guessed > 0)
                {
                    Console.Clear();
                    Console.WriteLine(render[7 - errorCount]);
                    Console.WriteLine($"Слово состоит из {word.Length} букв");
                    Console.WriteLine($"Угадал! осталось попыток {errorCount}");                    
                }
                else
                {                                       
                    Console.Clear();
                    Console.WriteLine(render[7-errorCount]);
                    errorCount--;
                    Console.WriteLine($"Слово состоит из {word.Length} букв");
                    Console.WriteLine($"Нет такой буквы, осталось попыток {errorCount}");                    
                }

                Console.WriteLine(viewWord);
            }
            if (errorCount == 0)
            {
                Console.Clear();
                Console.WriteLine(render[6]);
                Console.WriteLine("Ты проиграл!");
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Ты выйграл!");
            }

            Console.WriteLine($"Желаете продолжить да/нет");
            string str = Console.ReadLine();
            if (str == "да")
            {
                continue;
            }
            break;
        }
    }
}
